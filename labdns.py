try:
    import config
except ImportError as e:
    print "Please copy config.example.py to config.py and change it."
    raise e

import logging
import sys

import lib


def main():
    # Init logging.
    lh = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - '
                                  '%(levelname)s - %(message)s')
    lh.setFormatter(formatter)
    log = logging.getLogger(__name__)
    lh.setLevel(config.LOGLEVEL)
    log.setLevel(config.LOGLEVEL)
    log.addHandler(lh)
    logging.getLogger('lib.spider').addHandler(lh)
    logging.getLogger('lib.macgrabber').addHandler(lh)
    logging.getLogger('lib.db').addHandler(lh)
    logging.getLogger('lib.dnspush').addHandler(lh)

    results = lib.macgrabber(config.RANGE, config.CURTIN_ID, config.PASSWORD)
    lib.db.write(config.SQLITE_DB_PATH, config.MAPPINGS_FILE_LOCATION, results)
    rows = lib.db.read(config.SQLITE_DB_PATH)
    rows = [row for row in rows if row['Room'] != None]
    lib.dnspush.mass_update_records(
        config.CLOUDFLARE_USERNAME, config.CLOUDFLARE_API_KEY, rows,
        config.CLOUDFLARE_ZONE, config.CLOUDFLARE_RECORD_TTL)
if __name__ == '__main__':
    main()
