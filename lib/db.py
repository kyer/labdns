import sqlite3
import logging
import csv

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def create_db(db_path):
    # Set up the database.
    log.info("Creating host database if it doesn't already exist.")
    db = sqlite3.connect(db_path)
    db.execute("CREATE TABLE IF NOT EXISTS 'hosts' ("
               "MAC TEXT PRIMARY KEY,"
               "IP TEXT NOT NULL,"
               "ROOM INTEGER,"
               "MACHINE INTEGER,"
               "LAST_UPDATED TEXT NOT NULL)")
    db.commit()


def write(db_path, mappings, results):
    create_db(db_path)
    db = sqlite3.connect(db_path)

    log.info("Writing records to database.")
    csvfile = open(mappings, 'rb')
    csvreader = csv.DictReader(csvfile)
    for r in results:
        mac = r['output'][0][0]
        host = r['host']
        log.debug("Writing row {mac} -> {host}".format(mac=mac, host=host))
        db.execute("INSERT OR REPLACE INTO 'hosts' (MAC, IP, LAST_UPDATED)"
                   "VALUES (?, ?, datetime())", (mac, host))
        db.commit()
        # Check for physical location info in mappings file and write that
        # to DB too.
        physical_info = [item for item in csvreader if item['MAC'] == mac]
        for phys in physical_info:
            room = phys['Room']
            machine = phys['Machine']
            log.debug("Updating IP address in for {room}-{machine} in the "
                      "database.".format(room=room, machine=machine))
            db.execute("UPDATE 'hosts' SET ROOM = ?, MACHINE = ? WHERE "
                       "MAC = ?", (room, machine, mac))
        # Apparently this doesn't happen automatically...FIXME
        csvfile.seek(0)

    log.info("Committing to database.")
    db.commit()
    log.info("Closing database.")
    db.close()


def read(db_path):
    db = sqlite3.connect(db_path)
    db.row_factory = sqlite3.Row
    log.info("Reading records from database.")

    c = db.cursor()
    c.execute("SELECT * FROM 'hosts'")
    return c.fetchall()
