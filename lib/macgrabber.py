import logging
import spider

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

def macgrabber(host_range, username, password):
    # Get MAC addresses
    retrieve_command = ("/bin/bash -c \"/sbin/ifconfig eth0 "
                        "| grep 'HWaddr '| cut -d ' ' -f11 | tr -d '\n'\"")

    log.debug("Fetching MAC addresses.")
    return spider.mass_execute(
        host_range, retrieve_command, username, password)
