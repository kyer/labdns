import logging

from pyflare import PyflareClient

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def mass_update_records(username, api, hosts, zone_name, ttl):
    log.debug("Writing DNS records.")
    cf = PyflareClient(username, api)
    zone = cf.rec_load_all(zone_name)

    for host in hosts:
        hostname = "{room}-{:0>2d}".format(
            host['Machine'], room=host['Room'])
        full_host = "{host}.{domain}".format(host=hostname, domain=zone_name)
        records = [record for record in zone if record['name'] == full_host]

        for record in records:
            log.debug("Deleting old record: {r[name]} ({r[rec_id]})"
                      .format(r=record))
            cf.rec_delete(zone_name, record['rec_id'])

        log.debug("Writing record: {hostname} -> {ip}".format(
            hostname=hostname, ip=host['IP']))
        cf.rec_new(zone=zone_name, record_type='A', name=hostname,
                   content=host['IP'], ttl=ttl)
