import paramiko
import socket
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def remote_execute(host, commands, username, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname=host, username=username, password=password,
                   timeout=1)

    results = list()

    if not hasattr(commands, '__iter__'):
        commands = (commands,)

    for command in commands:
        results.append(client.exec_command(command)[1].readlines())

    return results


def mass_execute(hosts, commands, username, password):
    hosts_iter = ([(x if hasattr(x, '__iter__')
                    else (x,)) for x in hosts])

    # I'm trading effeciency for clean code here.
    hosts = list()
    for a in hosts_iter[0]:
        for b in hosts_iter[1]:
            for c in hosts_iter[2]:
                for d in hosts_iter[3]:
                    hosts.append("{a}.{b}.{c}.{d}".format(a=a, b=b, c=c, d=d))

    results = list()
    for host in hosts:
        log.debug("Logging in to host: {host}.".format(host=host))
        try:
            results.append({'host': host, 'output': remote_execute(
                host, commands, username, password)})
            log.info("Successfully executed on {host}.".format(host=host))
        except paramiko.ssh_exception.AuthenticationException:
            log.debug("Authentication error with host {host}"
                      .format(host=host))
        except socket.timeout:
            log.debug("Connection timed out with host {host}."
                      .format(host=host))
        except paramiko.ssh_exception.SSHException:
            log.debug("General SSH error with {host}."
                      .format(host=host))
        except EOFError:
            log.debug("Socket ended with {host}."
                      .format(host=host))
        except socket.error:
            log.debug("Socket error with {host}"
                      .format(host=host))
    return results
