#
# Configuration file for labdns
#

import logging

# IP range of lab machines
# Format: (octet 1 choices, octet 2 choices, octet 3 choices, octet 4 choices).
# Hint: Use range()
RANGE = (1, 2, 3, 4)

# Curtin details (for login)
CURTIN_ID = '1000000000'
PASSWORD = 'CoolShirtz15'

# Database details
SQLITE_DB_PATH = 'db.sqlite3'

# Console logging verbosity
LOGLEVEL = logging.DEBUG

# Physical mapping
MAPPINGS_FILE_LOCATION = 'mappings.csv'

# CloudFlare Details
CLOUDFLARE_USERNAME = 'ayyy.lmao@curtin.edu.au'
CLOUDFLARE_API_KEY = '1313131313131313131313131313131313'
CLOUDFLARE_ZONE = 'secret.club'
CLOUDFLARE_RECORD_TTL = 3600
